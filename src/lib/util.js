const R = require('ramda');
const uniqueSlug = require('unique-slug');
const slugify = require('slugify');

const renameKeys = R.curry((keysMap, obj) =>
  R.reduce(
    (acc, key) => R.assoc(keysMap[key] || key, obj[key], acc),
    {},
    R.keys(obj)
  )
);

const generateUniqueSlug = name =>
  `${slugify(name, {
    replacement: '-',
    remove: null,
    lower: true,
  })}-${uniqueSlug()}`;

const generateSlug = name =>
  `${slugify(name, {
    replacement: '-',
    remove: null,
    lower: true,
  })}`;

module.exports = {
  renameKeys,
  generateUniqueSlug,
  generateSlug,
};
