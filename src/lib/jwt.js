const jwt = require('jsonwebtoken');

module.exports.decode = token => {
  const decoded = jwt.decode(token, { complete: true });
  return decoded;
};
