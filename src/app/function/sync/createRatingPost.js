const R = require('ramda');
const moment = require('moment');
const jwtHelper = require('../../../lib/jwt');
const { Unauthorization } = require('../../errors');
const logger = require('../../../core/logger');
const { NotFound } = require('../../errors');
const userSrv = require('../../../service/user.service');
const postSrv = require('../../../service/post.service');
const ratingSrv = require('../../../service/rating.service');
// const broker = require('../../../core/amqpBroker.core');

module.exports = async ctx => {
  try {
    logger.info(`------------- create rating post ----------`);

    const accessToken = ctx.headers.authorization
      ? ctx.headers.authorization.slice(7)
      : Unauthorization();

    const { payload } = await jwtHelper.decode(accessToken);
    const { object_id: objectId, value } = ctx.request.body;

    const userId = payload.id;

    const postData = await postSrv.findOnePost(objectId);

    const userData = !postData
      ? NotFound(['a post'])
      : await userSrv.getUserById(userId);

    const insertObject = !userData
      ? NotFound(['user'])
      : {
          user_id: userId,
          object_id: objectId,
          ...R.pick(['user_name', 'avatar_url'], userData),
          ...R.pick(['short_description', 'name', 'object_type'], postData),
          rate_value: value,
          rated_at: new Date(moment.utc().toISOString()),
        };

    // eslint-disable-next-line
    const insertResult = !postData
      ? NotFound(['a post'])
      : await ratingSrv.findOneAndUpdateRating(insertObject);

    // await broker.getPublishChannel().publish(
    //   'rating-ex',
    //   `${postType}.post.${itemResp.object_type}.like.created`,
    //   Buffer.from(
    //     JSON.stringify({
    //       user_id,
    //       object_id,
    //       object_type: itemResp.object_type,
    //       ...R.pick(['user_name', 'user_avatar', 'user_link'], userInfo),
    //       ...R.pick(
    //         [
    //           'short_description',
    //           'detail_description',
    //           'name',
    //           'page_link',
    //           'page_title',
    //           'company_id',
    //           'cover_image',
    //         ],
    //         itemResp
    //       ),
    //       like_value: value,
    //       liked_at: likedAt,
    //     }),
    //     'utf8'
    //   )
    // );

    ctx.status = 204;
    ctx.body = undefined;
  } catch (error) {
    ctx.throw(error);
  }
};
