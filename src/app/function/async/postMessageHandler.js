const logger = require('../../../core/logger');
const postSrv = require('../../../service/post.service');

module.exports = async message => {
  try {
    const messageJson = JSON.parse(message.content);

    const postType = message.fields.routingKey('.')[0];

    const insertResult = await postSrv.findOneAndUpdatePost({
      ...messageJson,
      post_type: postType,
    });

    logger.info(
      `post ${insertResult.value.object_id} inserted: ${!insertResult
        .lastErrorObject.updatedExisting}`
    );
  } catch (error) {
    logger.log(error);
  }
};
