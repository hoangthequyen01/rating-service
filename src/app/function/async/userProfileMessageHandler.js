const R = require('ramda');
const logger = require('../../../core/logger');
const userSrv = require('../../../service/user.service');

module.exports = async message => {
  try {
    const messageJson = JSON.parse(message.content);

    const insertObject = R.pick(
      [
        'user_id',
        'user_name',
        'name',
        'phone_number',
        'location',
        'avatar_url',
        'about_me',
        'banner_url',
        'email',
        'created_at',
        'updated_at',
      ],
      messageJson
    );

    const insertResult = await userSrv.findOneAndUpdateUserProfile(
      insertObject
    );

    logger.info(
      `user ${insertResult.value.user_id} inserted: ${!insertResult
        .lastErrorObject.updatedExisting}` //eslint-disable-line
    );
  } catch (error) {
    logger.log(error);
  }
};
