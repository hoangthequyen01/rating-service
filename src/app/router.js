const Router = require('koa-router');
const validator = require('../middlewares/validator');
const schema = require('./schema');
const handler = require('./handler');

const router = new Router();

// POST METHOD
router.post(
  '/ratings',
  validator(schema.createRatingPost)(),
  handler.createRatingPost
);

module.exports = router;
