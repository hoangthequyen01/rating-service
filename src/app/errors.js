const createError = require('http-errors');

module.exports = {
  NotFound: objects => {
    throw createError(404, 'not found', {
      errors: objects.map(object => ({
        code: 404,
        message: `${object} not found`,
      })),
    });
  },
  UniqueViolation: objects => {
    throw createError(422, 'key already exists', {
      errors: objects.map(object => ({
        code: 422,
        message: `Key ${object} already exists`,
      })),
    });
  },
  Unauthorization: () => {
    throw createError(401, 'unauthorization', {
      errors: [
        {
          code: 401,
          message: 'unauthorization',
        },
      ],
    });
  },
  SomethingError: messages => {
    throw createError(400, 'something errors', {
      errors: messages.map(m => ({
        code: m.code,
        message: m.message,
      })),
    });
  },
};
