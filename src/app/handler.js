// ASYNC ACTION
const userProfileMessageHandler = require('../app/function/async/userProfileMessageHandler');
const postMessageHandler = require('./function/async/postMessageHandler');

// SYNC ACTION
const createRatingPost = require('./function/sync/createRatingPost');

module.exports = {
  userProfileMessageHandler,
  postMessageHandler,
  createRatingPost,
};
