/* -------- BODY --------- */
const createRatingPost = {
  type: 'object',
  additionalProperties: false,
  required: ['value', 'object_id'],
  properties: {
    value: { type: 'number', enum: [0, 1, 2, 3, 4, 5] },
    object_id: { type: 'string', minLength: 10 },
  },
};

module.exports = {
  createRatingPost,
};
