const { MongoClient } = require('mongodb');

const state = {
  db: null,
  client: null,
};

const connect = (mongoUrl, { logger = console }) =>
  new Promise((resolve, reject) => {
    if (state.db) resolve(state.db);

    return MongoClient.connect(
      mongoUrl,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      (err, client) => {
        if (err) {
          logger.error('[MONGO] connected failure');
          reject(err);
        }
        // set connection to state.connection
        state.db = client.db();
        state.client = client;
        logger.info('[MONGO] connected to: ', mongoUrl);
        resolve(state.client);
      }
    );
  });

const close = () =>
  // eslint-disable-next-line
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    state.db.close((err, result) => {
      state.db = null;
      resolve();
    });
  });

const db = () => state.db;

const client = () => state.client;

module.exports = {
  db,
  client,
  connect,
  close,
};
