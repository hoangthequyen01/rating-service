const http = require('http');
const config = require('config');
const router = require('./app/router');
const logger = require('./core/logger');
const app = require('./core/koa.core')(router, logger);
const rabbitConf = require('./core/rabbit.core');
const broker = require('./core/amqpBroker.core');
const handler = require('./app/handler');
const mongoConf = require('./core/mongo.core');

const startApiServer = () =>
  new Promise((resolve, reject) => {
    http.createServer(app.callback()).listen(config.get('port'), err => {
      if (err) reject(err);
      resolve();
    });
  });

const start = async () => {
  try {
    await rabbitConf.connect(config.get('rabbitUrl'), { logger });
    await broker.init(rabbitConf.connection(), config.get('amqpDefinitions'));

    Promise.all([
      mongoConf.connect(config.get('mongoUrl'), { logger }),

      broker.addConsumer(
        'rating-user-profile-q',
        handler.userProfileMessageHandler,
        { noAck: true }
      ),

      broker.addConsumer('rating-post-q', handler.postMessageHandler, {
        noAck: true,
      }),

      startApiServer(),
    ]);

    logger.info(
      `${'[MAIN]'} Server is listening on port ${config.get('port')}`
    );
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
};

start();

const shutdown = signal => async err => {
  logger.log(`${signal}...`);
  if (err) logger.error(err.stack || err);

  await mongoConf.close();
  await rabbitConf.close();
  logger.info(`${signal} signal received.`);
  process.exit(1);
};

process.on('SIGTERM', shutdown('SIGNTERM'));
