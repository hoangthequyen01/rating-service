const path = require('path');
const protoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');
const pify = require('pify');
const config = require('config');

const PROTO_PATH = path.resolve(__dirname, '../protos/userProfile.proto');

const pd = protoLoader.loadSync(PROTO_PATH);
const loaded = grpc.loadPackageDefinition(pd);
// eslint-disable-next-line
const { user_profile } = loaded;
// eslint-disable-next-line
const client = new user_profile.userService(
  config.get('userProfileGrpcUrl'),
  grpc.credentials.createInsecure()
);

module.exports.getUserById = userId =>
  pify(client).getUserById({
    userId,
  });

// main();
