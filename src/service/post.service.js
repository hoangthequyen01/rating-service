const commonSvc = require('./common.service');

const COLLECTION_NAME = 'posts';

/* ---------- COMMON FUNCTION ---------- */
const findOneAndUpdateCommon = commonSvc.findOneAndUpdate(COLLECTION_NAME);
const findOnecommon = commonSvc.findOne(COLLECTION_NAME);

/* ------------- USER FUNCTION ------------- */

const findOnePost = postId => findOnecommon({ object_id: postId });

const findOneAndUpdatePost = post =>
  findOneAndUpdateCommon({
    object_id: post.object_id,
  })({
    $set: post,
  })({ upsert: true, returnOriginal: false });

module.exports = {
  findOnePost,
  findOneAndUpdatePost,
};
