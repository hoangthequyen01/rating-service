const { db } = require('../core/mongo.core');

module.exports.insertOne = colection => data =>
  db()
    .collection(colection)
    .insertOne(data);

module.exports.findOneAndUpdate = colection => filter => update => options =>
  db()
    .collection(colection)
    .findOneAndUpdate(filter, update, options);

module.exports.findOne = colection => filter =>
  db()
    .collection(colection)
    .findOne(filter);

module.exports.updateOne = colection => filter => update => options =>
  db()
    .collection(colection)
    .updateOne(filter, update, options);

// module.exports.deleteOne = async (filter, options) => {
//   try {
//     const db = mongo.db();
//     const coll = db.collection(COLLECTION_NAME);

//     const deleteResult = await coll.deleteOne(filter, options);

//     if (deleteResult.deletedCount === 0) throw errors.UserPostNotFound();

//     if (deleteResult.result.ok !== 1) throw errors.UnexpectedError();

//     return Promise.resolve(Result.Ok(true));
//   } catch (error) {
//     return Promise.resolve(Result.Error(error));
//   }
// };
