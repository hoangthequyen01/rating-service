const commonSvc = require('./common.service');

const COLLECTION_NAME = 'ratings';

/* ---------- COMMON FUNCTION ---------- */
const findOneAndUpdateCommon = commonSvc.findOneAndUpdate(COLLECTION_NAME);

/* ------------- USER FUNCTION ------------- */

const findOneAndUpdateRating = object =>
  findOneAndUpdateCommon({
    object_id: object.object_id,
  })({
    $set: object,
  })({ upsert: true, returnOriginal: false });

module.exports = {
  findOneAndUpdateRating,
};
