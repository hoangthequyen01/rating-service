FROM node:12.12.0

WORKDIR /home/app
ENV NODE_ENV=development \
    PORT=8000 \
    HOST=localhost  
    
COPY package.json /home/app
COPY package-lock.json /home/app

RUN npm install

COPY . /home/app

EXPOSE 8000

CMD ["node", "./src/app.js"]


